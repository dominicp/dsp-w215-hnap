/**
 * Keep helper functions here
 */
const { format } = require('util');

exports.wait = (delay) => {

    let timer

    const promise = new Promise((resolve) => {

         timer = setTimeout(resolve, delay);
    });

    const cancel = () => { clearTimeout(timer); };

    return [promise, cancel];
};

exports.identity = (arg) => arg;

const getWriter = (channel) => (...message) => {
    process[channel].write(new Date().toLocaleString() + ' ' + format(...message) + '\n');
};

exports.writeOut = getWriter('stdout');

exports.writeErr = getWriter('stderr');
