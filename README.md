dsp-w215-hnap
=============
Tool for reading data from D-Link DSP-W215 Home Smart Plug.
Tested with hardware version B1 and firmware version 2.20.
See [wiki](https://github.com/bikerp/dsp-w215-hnap/wiki) for more information.

I have modified the original to move the config to a separate file and make it
more of a CLI type app. All of the hard work and credit goes to the
[original author](https://github.com/bikerp). Thanks so much for figuring out
how this works.


Usage
-----
1. Clone the project: `git clone https://gitlab.com/dominicp/dsp-w215-hnap.git`

2. Install `npm install`.

3. Create a development.json file in the config folder with details filled in
based on the example.json file. The name of the config file is based on the value
of the `NODE_ENV` variable and if that variable is not set, it defaults to
"development.json".

4. Run the app: `node app.js [command] [tries]`

If the second `tries` parameter is passed, the command will be retried that many
times on failure before giving up. So: `node app.js on 50`, will attempt to turn
the switch on 50 times, with a delay between each try, before it fails.


Commands
--------
- on: Turn the switch on
- off: Turn the switch off
- state: Get the current state: `ON`, `OFF`, `UNKNOWN`
- consumption: Get the current power consumption
- temp: Get the current temperature
- connect: Use the settings in the config file to connect to a WiFi network
- survey: Run a WiFi site survey and return the results
- help: Prints usage information
