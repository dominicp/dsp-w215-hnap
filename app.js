/**
 * Tool for reading data from D-Link DSP-W215 Home Smart Plug.
 *
 * Usage: enter your PIN code to LOGIN_PWD, change value of HNAP_URL according to your device settings.
 *
 * @type {exports|module.exports}
 */
var config = require('config-bark').default;
var soapclient = require('./js/soapclient');
var { identity, wait, writeOut, writeErr } = require('./js/util');

var LOGIN_USER = 'admin';
var LOGIN_PWD = config.pin();
var HNAP_URL = config.url('http://192.168.1.128') + '/HNAP1';
var RETRY_DELAY = 15000;

// Get the command
var command = process.argv[2];

// Get the number of tries before we give up
var tries = parseInt(process.argv[3], 10) || 1;
tries -= 1;

// If no command was given, print usage and exit
if (! command || command === 'help') {

    if (! command) { writeErr('No command given.'); }
    usage();
    process.exit(1);
}

// Run the command
switch (command) {

    case 'state':
        run('state', (result) => {

            const responseMap = { true: 'ON', false: 'OFF' };

            return responseMap[result] || 'UNKNOWN';
        });
        break;

    case 'survey':
        run('triggerWirelessSiteSurvey', () => '')
            .then((waitTime) => {

                let delay = parseInt(waitTime, 10);

                if (! delay || Number.isNaN(delay)) {

                    writeErr('Warning: Failed to get estimated wait time for site survey.');
                    delay = 15;
                }

                // Set a cap on the wait time
                delay = Math.min(delay, 20);

                writeOut('Waiting ' + delay + ' sec. for site survey to complete...');

                return wait(delay * 1000)[0];
            })
            .then(() => run('getSiteSurvey', () => ''))
            .then((nodeList) => {

                if (! nodeList || nodeList.length < 1) {

                    writeErr('Failed to get site survey results.');
                    return;
                }

                // Loop through the list of APs
                for (let i = 0; i < nodeList.length; i++) {

                    outputAPNode(nodeList[i]);
                    writeOut('----------');
                }
            });
        break;

    case 'connect':
        run('setAPClientSettings')
        break;

    case 'temp':
        run('temperature');
        break;

    default:
        run(command);
}

function outputAPNode(apNode) {

    const apDetails = new Map([
        ['SSID', apNode.getElementsByTagName('SSID')],
        ['MAC Address', apNode.getElementsByTagName('MacAddress')],
        ['Signal Strength', apNode.getElementsByTagName('SignalStrength')],
        ['Security', apNode.getElementsByTagName('SecurityType')]
    ]);

    apDetails.forEach((nodeList, label) => {

        const node = nodeList.item(0);

        const value = (node && node.firstChild) ? node.firstChild.nodeValue : 'UNKNOWN';

        writeOut(label + ': ' + value);
    });
}

function run(command, formatOutput = identity, triesRemaining = tries) {

    writeOut('Running command: ' + command);

    const [waitPromise, cancel] = wait(8000);

    return Promise.race([
        login()
            .then(() => {

                return soapclient[command]();
            })
            .then((result) => {

                cancel();

                writeOut(formatOutput(result));

                return result;
            }),
        waitPromise.then(() => { throw new Error('Command timed out.'); })
    ])
        .catch((error) => {

            cancel();

            writeErr('Error running command', error);

            if (triesRemaining > 0) {

                writeOut('Retrying command ' + triesRemaining + ' more time.');

                return wait(RETRY_DELAY)[0]
                    .then(() => run(command, formatOutput, (triesRemaining - 1)));
            }

            writeErr('No more tries remaining. Exiting...');

            process.exit(3);
        });
}

function login() {

    return soapclient.login(LOGIN_USER, LOGIN_PWD, HNAP_URL)
        .then((status) => {

            if (status && status === 'success') { return; }

            writeErr('Failed to login.', status);
            process.exit(2);
        });
}

function usage() {

    writeErr(
        '\nUsage: node app.js [command] [tries]\n\n'
            + 'Commands:\n'
            + 'on: Turn the switch on\n'
            + 'off: Turn the switch off\n'
            + 'state: Get the current state: ON, OFF, UNKNOWN\n'
            + 'consumption: Get the current power consumption\n'
            + 'temp: Get the current temperature\n'
            + 'connect: Use the settings in the config file to connect to the WiFi network\n'
            + 'survey: Run a WiFi site survey and return the results\n'
            + 'help: Print this help message\n\n'
            + 'Tries: How many times to try the command before giving up.'
    );
}
